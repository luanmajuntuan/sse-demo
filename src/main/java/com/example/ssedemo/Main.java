package com.example.ssedemo;

import static org.asynchttpclient.Dsl.asyncHttpClient;
import static org.asynchttpclient.Dsl.config;

import io.netty.util.concurrent.Future;
import java.util.concurrent.ExecutionException;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.Dsl;
import org.asynchttpclient.netty.ws.NettyWebSocket;
import org.asynchttpclient.ws.WebSocket;
import org.asynchttpclient.ws.WebSocketListener;
import org.asynchttpclient.ws.WebSocketUpgradeHandler.Builder;

public class Main {

  public static void main(String[] args) throws ExecutionException, InterruptedException {
    WebSocket websocket =  Dsl.asyncHttpClient().prepareGet("wss://test-pconnect2.coolkit.cc:8080/api/ws")
        .execute(new Builder().addWebSocketListener(
            new WebSocketListener() {

              @Override
              public void onOpen(WebSocket websocket) {
                websocket.sendTextFrame("{\n"
                    + "\t\"action\": \"userOnline\",\n"
                    + "\t\"version\": \"4\",\n"
                    + "\t\"ts\": 1534140442,\n"
                    + "\t\"os\": \"android\",\n"
                    + "\t\"userAgent\": \"app\",\n"
                    + "\t\"apikey\": \"6b07b867-1cb1-49db-9dbd-a16e2d333576\",\n"
                    + "\t\"sequence\": \"1534140442207\",\n"
                    + "\t\"appid\": \"1xMdjbmOBYctEJfye4EjFLR2M6YpYyyJ\",\n"
                    + "\t\"nonce\": \"ch16ifk8\",\n"
                    + "\t\"apkVesrion\": \"3.2.1.1094040_20180127_18\",\n"
                    + "\t\"at\": \"eb0d669b9b25ebb7f4c69c859241d402ec9e62ee\",\n"
                    + "\t\"model\": \"MEIZU M6Meizu\",\n"
                    + "\t\"imei\": \"867182031860420\",\n"
                    + "\t\"romVersion\": \"7.0\"\n"
                    + "}");
              }

              @Override
              public void onClose(WebSocket websocket, int code, String reason) {
                System.out.println(reason);
              }

              @Override
              public void onTextFrame(String payload, boolean finalFragment, int rsv) {
                System.out.println(payload);
              }

              @Override
              public void onError(Throwable t) {
                System.out.println(t);
              }
            }).build()).get();
    if (websocket.isOpen()) {
    //  websocket.sendTextFrame("{\"action\":\"query\",\"apikey\":\"6b07b867-1cb1-49db-9dbd-a16e2d333576\",\"deviceid\":\"a01000003e\",\"userAgent\":\"app\",\"params\":[],\"sequence\":\"1534140442555\"}");
    websocket.sendTextFrame("ping");
    }

}

}

